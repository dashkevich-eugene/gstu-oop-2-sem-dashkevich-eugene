﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib_Lab2;

namespace Lab2_App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            LoadDetective();
        }

        private void LoadDetective()
        {
            Detective.Detectives = Detective.LoadDetective();
            listBox1.Items.Clear();

            foreach (Detective detective in Detective.Detectives)
            {
                listBox1.Items.Add(detective);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                propertyGrid1.SelectedObject = listBox1.SelectedItem;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(textBox1.Text) && 
                DateTime.TryParse(textBox2.Text, out _) &&
                !string.IsNullOrEmpty(textBox3.Text) && 
                double.TryParse(textBox4.Text, out _))
            {
                Detective.AddNewDetective(textBox1.Text, DateTime.Parse(textBox2.Text),
                    textBox3.Text, double.Parse(textBox4.Text));

                LoadDetective();
            }
            else
            {
                MessageBox.Show("Неправильно введены данные!");
            }

            textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = "";
        }
    }
}
