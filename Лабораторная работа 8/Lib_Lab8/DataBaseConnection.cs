﻿using System;
using System.Linq;
using System.Data.Linq;
using System.Collections.Generic;

namespace Lib_Lab8
{
    public class DataBaseConnection
    {
        public string ConnectionString;

        public DataBaseConnection(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public List<Detective> GetInfoDetective()
        {
            List<Detective> Detectives = new List<Detective>();

            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                Table<Detective> DataBaseDetectives = DataBase.GetTable<Detective>();

                foreach (Detective detective in DataBaseDetectives)
                {
                    Detectives.Add(detective);
                }
            }

            return Detectives;
        }

        public List<Violation> GetInfoViolation()
        {
            List<Violation> Violations = new List<Violation>();

            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                Table<Violation> DataBaseViolations = DataBase.GetTable<Violation>();

                foreach (Violation violation in DataBaseViolations)
                {
                    Violations.Add(violation);
                }
            }

            return Violations;
        }

        public List<ViolationsAction> GetInfoViolationsAction()
        {
            List<ViolationsAction> ViolationsActions = new List<ViolationsAction>();

            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                Table<ViolationsAction> DataBaseViolationsActions = DataBase.GetTable<ViolationsAction>();

                foreach (ViolationsAction violationsAction in DataBaseViolationsActions)
                {
                    ViolationsActions.Add(violationsAction);
                }
            }

            return ViolationsActions;
        }

        public void DeleteViolationsAction(int ViolationsActionID)
        {
            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                var DeleteViolationsAction = (from u in DataBase.GetTable<ViolationsAction>()
                                 where u.ViolationsActionID.ToString() == ViolationsActionID.ToString()
                                 select u).FirstOrDefault();

                DataBase.GetTable<ViolationsAction>().DeleteOnSubmit(DeleteViolationsAction);
                DataBase.SubmitChanges();
            }
        }

        public void UpdateViolationsAction(int ViolationsActionID, string LastNameObj)
        {
            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                var UpdateViolationsAction = (from u in DataBase.GetTable<ViolationsAction>()
                                 where u.ViolationsActionID.ToString() == ViolationsActionID.ToString()
                                 select u).FirstOrDefault();

                UpdateViolationsAction.ObjectLastName = LastNameObj;
                DataBase.SubmitChanges();
            }
        }

        public void AddNewViolationsAction(string LastName, int ViolationID)
        {
            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                ViolationsAction NewViolationsAction = new ViolationsAction() 
                { ObjectLastName = LastName, ViolationID = ViolationID };

                DataBase.GetTable<ViolationsAction>().InsertOnSubmit(NewViolationsAction);
                DataBase.SubmitChanges();
            }
        }
    }
}
