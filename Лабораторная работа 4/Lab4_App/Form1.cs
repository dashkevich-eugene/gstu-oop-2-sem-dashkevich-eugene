﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib_Lab4;

namespace Lab4_App
{
    public partial class ActionPicture : Form
    {
        Circle circle;
        ListView ColorList;
        TextBox SpeedTextBox;
        TextBox RadiusTextBox;
        bool IsExist = false;
        Graphics MoveObject;
        Color CurrentColor = Color.Black;

        public ActionPicture()
        {
            InitializeComponent();
            MoveObject = pictureBox.CreateGraphics();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Size = new Size(620, 650);
            this.Text = "Lab4";

            ColorList = new ListView();
            ColorList.Location = new Point(0, 0);
            ColorList.Size = new Size(100, 100);
            ColorList.MultiSelect = false;
            ColorList.Items.Add("R");
            ColorList.Items.Add("G");
            ColorList.Items.Add("B");
            ColorList.SelectedIndexChanged += ColorList_SelectedIndexChanged;
            this.Controls.Add(ColorList);

            Button Up = new Button();
            Up.Location = new Point(100, 0);
            Up.Size = new Size(400, 100);
            Up.Text = "Вверх";
            Up.Click += Up_Click;
            this.Controls.Add(Up);

            Button Down = new Button();
            Down.Location = new Point(100, 500);
            Down.Size = new Size(400, 100);
            Down.Text = "Вниз";
            Down.Click += Down_Click;
            this.Controls.Add(Down);

            Button Left = new Button();
            Left.Location = new Point(0, 100);
            Left.Size = new Size(100, 400);
            Left.Text = "Влево";
            Left.Click += Left_Click;
            this.Controls.Add(Left);

            Button Right = new Button();
            Right.Location = new Point(500, 100);
            Right.Size = new Size(100, 400);
            Right.Text = "Вправо";
            Right.Click += (o, el) => Right_Click(o, el);
            this.Controls.Add(Right);

            Button Create = new Button();
            Create.Location = new Point(0, 500);
            Create.Size = new Size(100, 100);
            Create.Text = "Создать";
            Create.Click += (o, el) => Create_Click(o, el); 
            this.Controls.Add(Create);

            Label SpeedText = new Label();
            SpeedText.Text = "Скорость:";
            SpeedText.Location = new Point(500, 0);
            this.Controls.Add(SpeedText);

            Label Radius = new Label();
            Radius.Text = "Радиус:";
            Radius.Location = new Point(500, 500);
            this.Controls.Add(Radius);

            SpeedTextBox = new TextBox();
            SpeedTextBox.Size = new Size(100, 30); 
            SpeedTextBox.Location = new Point(500, 30);
            this.Controls.Add(SpeedTextBox);

            RadiusTextBox = new TextBox();
            RadiusTextBox.Size = new Size(100, 30);
            RadiusTextBox.Location = new Point(500, 530);
            this.Controls.Add(RadiusTextBox);
        }

        private void ColorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ColorList.SelectedIndices.Count == 1)
            {
                int LastIndex = ColorList.SelectedIndices[0];

                switch (LastIndex)
                {
                    case 0:
                        CurrentColor = Color.Red;
                        break;
                    case 1:
                        CurrentColor = Color.Green;
                        break;
                    case 2:
                        CurrentColor = Color.Blue;
                        break;
                }

                if (IsExist)
                {
                    DrawPicture();
                }
            }
        }

        private void Create_Click(object sender, EventArgs e)
        {
            if(int.TryParse(SpeedTextBox.Text, out _) &&
                double.TryParse(RadiusTextBox.Text, out _) && double.Parse(RadiusTextBox.Text) < pictureBox.Width / 2)
            {
                circle = new Circle(pictureBox.Width / 2, pictureBox.Height / 2,double.Parse(RadiusTextBox.Text), 
                    int.Parse(SpeedTextBox.Text), int.Parse(SpeedTextBox.Text));

                DrawPicture();

                IsExist = true;
            }
            else
            {
                MessageBox.Show("Данные введены неправильно!");
            }

            SpeedTextBox.Text = RadiusTextBox.Text = "";
        }

        private void Right_Click(object sender, EventArgs e)
        {
            if(IsExist)
            { 
                circle.MoveOnAxisX(true, 0, pictureBox.Width);

                DrawPicture();
            }
            else
            {
                MessageBox.Show("Круг не создан!");
            }
        }

        private void Left_Click(object sender, EventArgs e)
        {
            if (IsExist)
            {
                circle.MoveOnAxisX(false, 0, pictureBox.Width);

                DrawPicture();
            }
            else
            {
                MessageBox.Show("Круг не создан!");
            }
        }

        private void Down_Click(object sender, EventArgs e)
        {
            if (IsExist)
            {
                circle.MoveOnAxisY(false, 0, pictureBox.Height);

                DrawPicture();
            }
            else
            {
                MessageBox.Show("Круг не создан!");
            }
        }

        private void Up_Click(object sender, EventArgs e)
        {
            if (IsExist)
            {
                circle.MoveOnAxisY(true, 0, pictureBox.Height);

                DrawPicture();
            }
            else
            {
                MessageBox.Show("Круг не создан!");
            }
        }

        private void DrawPicture()
        {
            DrawEllipse((float)circle.CenterX - (float)circle.Radius,
                    (float)circle.CenterY - (float)circle.Radius,
                    (int)circle.Radius * 2, (int)circle.Radius * 2);
        }

        private void DrawEllipse(float X, float Y, int Width, int Hieght)
        {
            MoveObject.Clear(Color.White);
            MoveObject.DrawEllipse(
                        new Pen(CurrentColor, 2f),
                        X, Y, Width, Hieght);
        }
    }
}
