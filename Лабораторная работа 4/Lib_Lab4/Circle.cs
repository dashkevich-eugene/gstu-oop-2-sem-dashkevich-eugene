﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab4
{
    public class Circle
    {
        public int CenterX { get; set; }
        public int CenterY { get; set; }
        public double Radius { get; set; }
        public double SpeedX { get; set; }
        public double SpeedY { get; set; }

        public Circle(int centerX, int centerY, double radius,
            double speedX, double speedY)
        {
            CenterX = centerX;
            CenterY = centerY;
            Radius = radius;
            SpeedX = speedX;
            SpeedY = speedY;
        }

        public void MoveOnAxisX(bool IsRight, int StartX, int Width)
        {
            if (IsRight && CenterX + Radius + SpeedX < StartX + Width)
            {
                CenterX += (int)SpeedX;
            }
            else if(!IsRight && CenterX - Radius - SpeedX > StartX)
            {
                CenterX -= (int)SpeedX;
            }
        }

        public void MoveOnAxisY(bool IsUp, int StartY, int Height)
        {
            if (IsUp && CenterY - Radius - SpeedY > StartY )
            {
                CenterY -= (int)SpeedY;
            }
            else if (!IsUp && CenterY + Radius + SpeedY < StartY + Height)
            {
                CenterY += (int)SpeedY;
            }
        }
    }
}
