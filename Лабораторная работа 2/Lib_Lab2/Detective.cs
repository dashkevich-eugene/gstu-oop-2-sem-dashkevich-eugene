﻿using System;
using System.Xml;
using System.Collections.Generic;

namespace Lib_Lab2
{
    public enum TypeOfViolation
    {
        Administrative,
        Criminal
    }

    public class Detective
    {
        public static List<Detective> Detectives;
        public string ObjectLastName { get; set; }
        public DateTime DateOfViolation { get; set; }
        public TypeOfViolation TypeOfViolation { get; set; }
        public double Price { get; set; }

        public Detective(string objectLastName, DateTime dateOfViolation,
            TypeOfViolation typeOfViolation, double price)
        {
            ObjectLastName = objectLastName;
            DateOfViolation = dateOfViolation;
            TypeOfViolation = typeOfViolation;
            Price = price;
        }

        public override string ToString()
        {
            return "Обект :" + ObjectLastName + " - сумма нарушения :" + Price;
        }

        public static List<Detective> LoadDetective()
        {
            //Создаем список журналов.
            List<Detective> newDetective = new List<Detective>();

            using (XmlReader XR = XmlReader.Create(@"C:\Users\User\source\repos\Labor_OOP_Semestr_2\Лабораторная работа 2\Lib_Lab2\XML.xml"))
            {
                string ObjectLastName = "";
                DateTime DateOfViolation = DateTime.Now;
                TypeOfViolation TypeOfViolation = TypeOfViolation.Administrative;
                double Price = 0;
                string element = "";

                while (XR.Read())
                {
                    // Считываем элемент.
                    if (XR.NodeType == XmlNodeType.Element)
                    {
                        //Получаем имя текущего элемента.
                        element = XR.Name;
                        if (element == "Detective")
                        {
                            ObjectLastName = XR.GetAttribute("ObjectLastName");
                        }
                    }
                    //Считывает значение элемента
                    else if (XR.NodeType == XmlNodeType.Text)
                    {
                        switch (element)
                        {
                            case "DateOfViolation":
                                DateOfViolation = DateTime.Parse(XR.Value);
                                break;
                            case "TypeOfViolation":
                                Enum.TryParse(XR.Value, out TypeOfViolation);
                                break;
                            case "Price":
                                Price = double.Parse(XR.Value);
                                break;
                        }
                    }
                    //Добавляет новые элемент в список.
                    else if ((XR.NodeType == XmlNodeType.EndElement) && (XR.Name == "Detective"))
                    {
                        newDetective.Add(new Detective(ObjectLastName, DateOfViolation, TypeOfViolation, Price)); ;
                    }
                }
            }

            return newDetective;
        }

        public static void AddNewDetective(string objectLastName, DateTime dateOfViolation,
            string typeOfViolation, double price)
        {
            // the XmlWriter settings
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            // writes the Detective
            using (XmlWriter xw = XmlWriter.Create(@"C:\Users\User\source\repos\Labor_OOP_Semestr_2\Лабораторная работа 2\Lib_Lab2\XML.xml", settings))
            {
                xw.WriteStartDocument(); // the header
                xw.WriteStartElement("root");

                foreach(Detective detective in Detectives)
                {
                    xw.WriteStartElement("Detective");
                    xw.WriteAttributeString("ObjectLastName", detective.ObjectLastName);
                    xw.WriteElementString("DateOfViolation", detective.DateOfViolation.ToString("d"));
                    xw.WriteElementString("TypeOfViolation", detective.TypeOfViolation.ToString());
                    xw.WriteElementString("Price", detective.Price.ToString());
                    xw.WriteEndElement();
                }

                xw.WriteStartElement("Detective");
                xw.WriteAttributeString("ObjectLastName", objectLastName);
                xw.WriteElementString("DateOfViolation", dateOfViolation.ToString("d"));
                xw.WriteElementString("TypeOfViolation", typeOfViolation.ToString());
                xw.WriteElementString("Price", price.ToString());
                xw.WriteEndElement();

                xw.WriteEndElement(); // closes the root element
                xw.WriteEndDocument(); // closes the document
                xw.Flush();
            }
        }
    }
}
