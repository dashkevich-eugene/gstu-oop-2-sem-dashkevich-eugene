﻿GO
CREATE DATABASE DetectiveCenter;

GO
USE DetectiveCenter;

Create table Detectives (DetectiveID int identity(1, 1) not null primary key,
LastName varchar(30), Name varchar(30), MiddleName varchar(30));

Create table Violations (ViolationID int identity(1, 1) not null primary key,
TypeOfViolation varchar(30), DateOfViolation Date, Fine int,
DetectiveID int foreign key references Detectives(DetectiveID));

Create table ViolationsActions (ViolationsActionID int identity(1, 1) not null primary key,
ObjectLastName varchar(30), ViolationID int foreign key references Violations(ViolationID));



