using System;
using Xunit;
using DoubleLinkedList;
using Lib_Lab2;

namespace DoubleLinkedListTests
{
    public class DoubleLinkedTest
    {
        [Fact]
        public void DoubleLinkedInt1()
        {
            //arrenge
            DoublyLinkedList<int> TestList = new DoublyLinkedList<int>();
            int FirstValue = 32;
            int SecondValue = 15;
            int expected = 15;
            //act
            TestList.Add(FirstValue);
            TestList.Add(SecondValue);
            int actual = TestList.head.Next.Data;
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DoubleLinkedInt2()
        {
            //arrenge
            DoublyLinkedList<int> TestList = new DoublyLinkedList<int>();
            int FirstValue = 32;
            int SecondValue = 15;
            int expected = 32;
            //act
            TestList.Add(FirstValue);
            TestList.Add(SecondValue);
            int actual = TestList.tail.Previous.Data;
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DoubleLinkedString1()
        {
            //arrenge
            DoublyLinkedList<String> TestList = new DoublyLinkedList<String>();
            String FirstValue = "32";
            String SecondValue = "15";
            String expected = "15";
            //act
            TestList.Add(FirstValue);
            TestList.Add(SecondValue);
            String actual = TestList.head.Next.Data;
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DoubleLinkedString2()
        {
            //arrenge
            DoublyLinkedList<String> TestList = new DoublyLinkedList<String>();
            String FirstValue = "32";
            String SecondValue = "15";
            String expected = "32";
            //act
            TestList.Add(FirstValue);
            TestList.Add(SecondValue);
            String actual = TestList.tail.Previous.Data;
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DoubleLinkedDetective1()
        {
            //arrenge
            DoublyLinkedList<Detective> TestList = new DoublyLinkedList<Detective>();
            Detective FirstValue = new Detective( "������", DateTime.Now, TypeOfViolation.Criminal, 50);
            Detective SecondValue = new Detective("������", DateTime.Now, TypeOfViolation.Criminal, 50);
            String expected = "������";
            //act
            TestList.Add(FirstValue);
            TestList.Add(SecondValue);
            String actual = TestList.head.Next.Data.ObjectLastName;
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DoubleLinkedDetective2()
        {
            //arrenge
            DoublyLinkedList<Detective> TestList = new DoublyLinkedList<Detective>();
            Detective FirstValue = new Detective("������", DateTime.Now, TypeOfViolation.Criminal, 50);
            Detective SecondValue = new Detective("������", DateTime.Now, TypeOfViolation.Criminal, 50);
            String expected = "������";
            //act
            TestList.Add(FirstValue);
            TestList.Add(SecondValue);
            String actual = TestList.tail.Previous.Data.ObjectLastName;
            //assert
            Assert.Equal(expected, actual);
        }
    }
}
