﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoubleLinkedList;

namespace Lab6_App
{
    public partial class Form1 : Form
    {
        DoublyLinkedList<string> linkedList = new DoublyLinkedList<string>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(textBox1.Text))
            {
                // добавление элементов
                linkedList.Add(textBox1.Text);
            }

            textBox1.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox2.Text))
            {
                // удаление
                linkedList.Remove(textBox2.Text);
            }

            textBox2.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            foreach (var t in linkedList.BackEnumerator())
            {
                listBox1.Items.Add(t);
            }
        }
    }
}
