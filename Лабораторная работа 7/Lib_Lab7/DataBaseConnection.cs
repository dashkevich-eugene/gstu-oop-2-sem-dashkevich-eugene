﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Lib_Lab7
{
    public class DataBaseConnection
    {
        public string ConnectionString;

        public DataBaseConnection(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public List<Detective> GetInfoDetective()
        {
            List<Detective> Detectives = new List<Detective>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Detectives", connection);

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Detectives.Add(new Detective(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)));
                    }
                }
            }

            return Detectives;
        }

        public List<Violation> GetInfoViolation()
        {
            List<Violation> Violations = new List<Violation>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Violations", connection);

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Violations.Add(new Violation(reader.GetInt32(0), reader.GetString(1), reader.GetDateTime(2), 
                            reader.GetInt32(3), reader.GetInt32(4)));
                    }
                }
            }

            return Violations;
        }

        public List<ViolationsAction> GetInfoViolationsAction()
        {
            List<ViolationsAction> ViolationsActions = new List<ViolationsAction>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM ViolationsActions", connection);

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ViolationsActions.Add(new ViolationsAction(reader.GetInt32(0), 
                            reader.GetString(1), reader.GetInt32(2)));
                    }
                }
            }

            return ViolationsActions;
        }

        public void DeleteViolationsAction(int ViolationsActionID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                string sqlExpression = "DELETE FROM ViolationsActions WHERE ViolationsActionID = @ViolationsActionID";
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                // создаем параметр для ID
                SqlParameter IdParam = new SqlParameter("@ViolationsActionID", ViolationsActionID);
                // добавляем параметр к команде
                command.Parameters.Add(IdParam);

                command.ExecuteNonQuery();
            }
        }

        public void UpdateViolationsAction(int ViolationsActionID, string LastNameObj)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlParameter FirstParam = new SqlParameter("@LastNameObj", LastNameObj);
                SqlParameter SecondParam = new SqlParameter("@ViolationsActionID", ViolationsActionID);
                string SqlExpression = "UPDATE ViolationsActions SET ObjectLastName = @LastNameObj" +
                    " WHERE ViolationsActionID = @ViolationsActionID";
                SqlCommand command = new SqlCommand(SqlExpression, connection);
                command.Parameters.Add(FirstParam);
                command.Parameters.Add(SecondParam);
                command.ExecuteNonQuery();
            }
        }

        public void AddNewViolationsAction(string LastName, int ViolationID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter lastName = new SqlParameter("@LastName", LastName);
                SqlParameter violationID = new SqlParameter("@ViolationID", ViolationID.ToString());

                string sqlExpression = "INSERT INTO ViolationsActions (ObjectLastName, ViolationID) VALUES " +
                    "(@LastName, @ViolationID);";
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.Add(lastName);
                command.Parameters.Add(violationID);

                command.ExecuteNonQuery();
            }
        }
    }
}
