﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib_Lab7;

namespace lab7_App
{
    public partial class Form1 : Form
    {
        DataBaseConnection connection = new DataBaseConnection("Data Source=DESKTOP-1T99363;Initial Catalog=DetectiveCenter;Integrated Security=True");
        List<ViolationsAction> ViolationsActions;
        List<Detective> Detectives;
        List<Violation> Violations;
        public Form1()
        {
            InitializeComponent();
            LoadViolationsActions();
            LoadViolation();
        }

        private void LoadViolationsActions()
        {
            ViolationsActions = new List<ViolationsAction>();
            ViolationsActions = connection.GetInfoViolationsAction();
            listBox1.Items.Clear();

            foreach (ViolationsAction violationsAction in ViolationsActions)
            {
                listBox1.Items.Add(violationsAction);
            }
        }
        public void LoadViolation()
        {
            Violations = new List<Violation>();
            Violations = connection.GetInfoViolation();
            comboBox1.Items.Clear();

            foreach (Violation violation in Violations)
            {
                comboBox1.Items.Add(violation);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                propertyGrid1.SelectedObject = listBox1.SelectedItem;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(int.TryParse(textBox1.Text, out _) && !string.IsNullOrEmpty(textBox2.Text))
            {
                connection.UpdateViolationsAction(int.Parse(textBox1.Text), textBox2.Text);
                LoadViolationsActions();
            }

            textBox1.Text = textBox2.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(int.TryParse(textBox3.Text, out _))
            {
                connection.DeleteViolationsAction(int.Parse(textBox3.Text));
                LoadViolationsActions();
            }

            textBox3.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(textBox4.Text) &&
                comboBox1.SelectedIndex >= 0 && comboBox1.SelectedIndex < comboBox1.Items.Count)
            {
                connection.AddNewViolationsAction(textBox4.Text, Violations[comboBox1.SelectedIndex].ViolationID);
                LoadViolationsActions();
            }

            textBox4.Text = "";
        }
    }
}
