﻿namespace StreamDecoratorApp
{
    partial class StreamDecoratorForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectFileBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TextOut = new System.Windows.Forms.Label();
            this.TextInput = new System.Windows.Forms.TextBox();
            this.WriteTextBtn = new System.Windows.Forms.Button();
            this.WrittenBytesCountOut = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SelectFileBtn
            // 
            this.SelectFileBtn.Location = new System.Drawing.Point(12, 234);
            this.SelectFileBtn.Name = "SelectFileBtn";
            this.SelectFileBtn.Size = new System.Drawing.Size(104, 23);
            this.SelectFileBtn.TabIndex = 0;
            this.SelectFileBtn.Text = "Записать в файл";
            this.SelectFileBtn.UseVisualStyleBackColor = true;
            this.SelectFileBtn.Click += new System.EventHandler(this.SelectFileBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Записанный текст:";
            // 
            // TextOut
            // 
            this.TextOut.AutoSize = true;
            this.TextOut.Location = new System.Drawing.Point(279, 81);
            this.TextOut.Name = "TextOut";
            this.TextOut.Size = new System.Drawing.Size(0, 13);
            this.TextOut.TabIndex = 4;
            // 
            // TextInput
            // 
            this.TextInput.Location = new System.Drawing.Point(12, 12);
            this.TextInput.Multiline = true;
            this.TextInput.Name = "TextInput";
            this.TextInput.Size = new System.Drawing.Size(261, 216);
            this.TextInput.TabIndex = 5;
            // 
            // WriteTextBtn
            // 
            this.WriteTextBtn.Location = new System.Drawing.Point(169, 234);
            this.WriteTextBtn.Name = "WriteTextBtn";
            this.WriteTextBtn.Size = new System.Drawing.Size(104, 23);
            this.WriteTextBtn.TabIndex = 7;
            this.WriteTextBtn.Text = "Записать";
            this.WriteTextBtn.UseVisualStyleBackColor = true;
            this.WriteTextBtn.Click += new System.EventHandler(this.WriteTextBtn_Click);
            // 
            // WrittenBytesCountOut
            // 
            this.WrittenBytesCountOut.AutoSize = true;
            this.WrittenBytesCountOut.Location = new System.Drawing.Point(279, 35);
            this.WrittenBytesCountOut.Name = "WrittenBytesCountOut";
            this.WrittenBytesCountOut.Size = new System.Drawing.Size(0, 13);
            this.WrittenBytesCountOut.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(279, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Количество записанных байт:";
            // 
            // ClearBtn
            // 
            this.ClearBtn.Location = new System.Drawing.Point(468, 53);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(104, 23);
            this.ClearBtn.TabIndex = 10;
            this.ClearBtn.Text = "Очистить";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // StreamDecoratorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 450);
            this.Controls.Add(this.ClearBtn);
            this.Controls.Add(this.WrittenBytesCountOut);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.WriteTextBtn);
            this.Controls.Add(this.TextInput);
            this.Controls.Add(this.TextOut);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SelectFileBtn);
            this.Name = "StreamDecoratorForm";
            this.Text = "StreamForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StreamDecoratorForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SelectFileBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label TextOut;
        private System.Windows.Forms.TextBox TextInput;
        private System.Windows.Forms.Button WriteTextBtn;
        private System.Windows.Forms.Label WrittenBytesCountOut;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ClearBtn;
    }
}

