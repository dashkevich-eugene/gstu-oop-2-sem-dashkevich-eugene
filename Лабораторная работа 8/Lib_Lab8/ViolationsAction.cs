﻿using System.Data.Linq.Mapping;

namespace Lib_Lab8
{
    [Table(Name = "ViolationsActions")]
    public class ViolationsAction
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int ViolationsActionID { get; set; }
        [Column(Name = "ObjectLastName")]
        public string ObjectLastName { get; set; }
        [Column(Name = "ViolationID")]
        public int ViolationID { get; set; }

        public override string ToString()
        {
            return ObjectLastName;
        }
    }
}
