﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;

namespace Lib_Lab8
{
    [Table(Name = "Violations")]
    public class Violation
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int ViolationID { get; set; }
        [Column(Name = "TypeOfViolation")]
        public string TypeOfViolation { get; set; }
        [Column(Name = "DateOfViolation")]
        public DateTime DateOfViolation { get; set; }
        [Column(Name = "Fine")]
        public int Fine { get; set; }
        [Column(Name = "DetectiveID")]
        public int DetectiveID { get; set; }

        public override string ToString()
        {
            return TypeOfViolation + " " + DateOfViolation;
        }
    }
}
