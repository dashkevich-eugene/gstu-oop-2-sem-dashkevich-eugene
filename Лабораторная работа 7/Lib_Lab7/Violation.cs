﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab7
{
    public class Violation
    {
        public int ViolationID { get; set; }
        public string TypeOfViolation { get; set; }
        public DateTime DateOfViolation { get; set; }
        public double Fine { get; set; }
        public int DetectiveID { get; set; }

        public Violation(int violationID, string typeOfViolation,
            DateTime dateOfViolation, double fine, int detectiveID)
        {
            ViolationID = violationID;
            TypeOfViolation = typeOfViolation;
            DateOfViolation = dateOfViolation;
            Fine = fine;
            DetectiveID = detectiveID;
        }

        public override string ToString()
        {
            return TypeOfViolation + " " + DateOfViolation;
        }
    }
}
