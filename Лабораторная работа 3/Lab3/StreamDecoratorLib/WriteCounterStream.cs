﻿using System.IO;

namespace StreamDecoratorLib
{
    public class WriteCounterStream : StreamDecorator
    {
        public int WrittenBytesCount { get; private set; }

        public WriteCounterStream(Stream stream) : base(stream)
        {
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            base.Write(buffer, offset, count);
            WrittenBytesCount += count;
        }
    }
}
