﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab7
{
    public class ViolationsAction
    {
        public int ViolationsActionID { get; set; }
        public string ObjectLastName { get; set; }
        public int ViolationID { get; set; }

        public ViolationsAction(int violationsActionID, string objectLastName,
            int violationID)
        {
            ViolationsActionID = violationsActionID;
            ObjectLastName = objectLastName;
            ViolationID = violationID;
        }

        public override string ToString()
        {
            return ObjectLastName;
        }
    }
}
