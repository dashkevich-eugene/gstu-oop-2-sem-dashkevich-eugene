﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Text;
using StreamDecoratorLib;

namespace StreamDecoratorApp
{
    public partial class StreamDecoratorForm : Form
    {
        private MemoryStream memory;
        private WriteCounterStream stream;
        private readonly Encoding encoding = new UTF8Encoding();

        public StreamDecoratorForm()
        {
            InitializeComponent();

            OpenStreams();
        }

        private void SelectFileBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "(*.xml;*.txt)|*.xml;*.txt|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                using (FileStream file = new FileStream(dialog.FileName, FileMode.Open, FileAccess.Write))
                using (BufferedStream buffered = new BufferedStream(file))
                using (WriteCounterStream stream = new WriteCounterStream(buffered))
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.Write(TextInput.Text);
                    writer.Flush();
                    WrittenBytesCountOut.Text = stream.WrittenBytesCount.ToString();
                }
            }
        }

        private void WriteTextBtn_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.Write(TextInput.Text);
            }
            DisplayData();
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            CloseStreams();
            OpenStreams();
            DisplayData();
        }

        private void StreamDecoratorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseStreams();
        }

        private void DisplayData()
        {
            using (StreamReader reader = new StreamReader(stream))
            {
                TextOut.Text = encoding.GetString(memory.ToArray());
            }
            WrittenBytesCountOut.Text = stream.WrittenBytesCount.ToString();
        }

        private void OpenStreams()
        {
            memory = new MemoryStream();
            stream = new WriteCounterStream(memory);
        }

        private void CloseStreams()
        {
            stream.Close();
            memory.Close();
        }
    }
}
