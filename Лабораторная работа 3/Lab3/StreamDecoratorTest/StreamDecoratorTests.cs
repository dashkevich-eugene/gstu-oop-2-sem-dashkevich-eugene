﻿using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StreamDecoratorLib;

namespace StreamDecoratorTest
{
    [TestClass]
    public class StreamDecoratorTests
    {
        [TestMethod]
        public void WriteCounterStreamReadTest()
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            string expected = "Lorem Ipsum Dolor";
            byte[] text = utf8.GetBytes(expected);

            using (MemoryStream memory = new MemoryStream(text))
            using (WriteCounterStream stream = new WriteCounterStream(memory))
            using (StreamReader reader = new StreamReader(stream))
            {
                Assert.AreEqual(expected, reader.ReadToEnd());
            }
        }

        [TestMethod]
        public void WriteCounterStreamWriteTest()
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            string expected = "Lorem Ipsum Dolor";

            using (MemoryStream memory = new MemoryStream())
            using (WriteCounterStream stream = new WriteCounterStream(memory))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.Write(expected);
                writer.Flush();

                Assert.AreEqual(expected, utf8.GetString(memory.ToArray()));
            }
        }

        [TestMethod]
        public void WriteCounterStreamCountTest()
        {
            string text = "Lorem Ipsum Dolor";
            int expected = 17;

            using (MemoryStream memory = new MemoryStream())
            using (WriteCounterStream stream = new WriteCounterStream(memory))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.Write(text);
                writer.Flush();

                Assert.AreEqual(expected, stream.WrittenBytesCount);
            }
        }
    }
}
