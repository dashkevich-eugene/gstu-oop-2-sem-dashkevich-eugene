﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab7
{
    public class Detective
    {
        public int DetectiveID { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }

        public Detective(int detectiveID, string lastName,
            string name, string middleName)
        {
            DetectiveID = detectiveID;
            LastName = lastName;
            Name = name;
            MiddleName = middleName;
        }

        public override string ToString()
        {
            return LastName + " " + Name + " " + MiddleName;
        }
    }
}
