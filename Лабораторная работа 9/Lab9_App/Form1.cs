﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab9_App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.AddRange(System.IO.File.ReadAllText(@"C:\Users\User\source\repos\Labor_OOP_Semestr_2\Лабораторная работа 9\Points.txt").Split('\n')
                .Select(x => { string[] Coords = x.Split(' '); return new { CoordX = double.Parse(Coords[0]), CoordY = double.Parse(Coords[1]) }; })
                .Select(Point => { return ("X: " + Point.CoordX.ToString() + "  Y: " + Point.CoordY.ToString()); }).ToArray());
        }
    }
}
